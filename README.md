# Les squelettes de seenthis.net

Ce plugin fait partie des éléments nécessaires à l'installation complète d'un site de type seenthis.net. Voir la procédure d'installation à l'adresse https://git.spip.net/seenthis/seenthis/-/blob/master/INSTALL.md

## Installation

Pour installer ces squelettes utilisez la commande suivante :

cd plugins/
git clone https://git.spip.net/seenthis/squelettes.git
```

## Signaler un problème

N'hésitez pas à signaler un problème en ouvrant une _issue_ sur https://git.spip.net/seenthis/squelettes/-/issues .
