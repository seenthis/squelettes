# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Fixed

- #296 `Warning: Undefined global variable $liste_id_me in (..)/squelettes/seenthissq_fonctions.php on line 354`
